# 常用 Markdown 語法

## 標題
```
# h1
## h2
### h3
#### h4
##### h5
###### h6
```

## 文字裝飾
- *斜體* `*斜體*` `_斜體_`
- **粗體** `**粗體**` `__粗體__`
- ***粗斜體*** `***粗斜體***` `___粗斜體___`
- ~~刪除線~~ `~~刪除線~~`
- [超連結](./) `[超連結](./)`

## 圖片
`![替代文字](https://s.pixfs.net/common/pixhappix-ui-module/images/btn-happix-ufo.png)`
![替代文字](https://s.pixfs.net/common/pixhappix-ui-module/images/btn-happix-ufo.png)
