# 初始化環境

- 安裝 gitbook-cli
    ```bash
    # 如果要 global 安裝可以加上 -g
    $ npm install gitbook-cli
    ```
- 初始化
    ```
    $ gitbook init
    ```
- 編譯並預覽
    ```
    $ gitbook serve
    ```

- Gitlab CI 自動部署 page
    - [Example GitBook website using GitLab Pages](https://gitlab.com/pages/gitbook)
    - [Gitlab page](https://ithelp.ithome.com.tw/articles/10197791)
    - 新增檔案 `.gitlab-ci.yml` 內容如下
    - 當有 commit 推上 master 時就會自動部署到 gitlab page

```yml
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
paths:
    - node_modules/ # Node modules and dependencies

before_script:
- npm install gitbook-cli -g # install gitbook
- gitbook fetch 3.2.3 # fetch final stable version
- gitbook install # add any requested plugins in book.json

test:
stage: test
script:
    - gitbook build . public # build to public path
only:
    - branches # this job will affect every branch except 'master'
except:
    - master

# the 'pages' job will deploy and build your site to the 'public' path
pages:
stage: deploy
script:
    - gitbook build . public # build to public path
artifacts:
    paths:
    - public
    expire_in: 1 week
only:
    - master # this job will affect only the 'master' branch
```
